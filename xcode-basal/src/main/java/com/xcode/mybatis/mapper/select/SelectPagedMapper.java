package com.xcode.mybatis.mapper.select;


import com.xcode.models.IPagedList;
import com.xcode.mybatis.mapper.Example;

public interface SelectPagedMapper {
    <TRecord> IPagedList<TRecord> SelectPaged(int pageSize, int pageIndex, Example example);
}
