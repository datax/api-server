package com.xcode.mybatis.mapper.select;

import java.util.List;

public interface SelectAllMapper{
    <TRecord> List<TRecord> selectAll();
}
