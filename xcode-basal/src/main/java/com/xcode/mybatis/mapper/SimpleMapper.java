package com.xcode.mybatis.mapper;

import com.xcode.mybatis.mapper.delete.DeleteByPrimaryKeyMapper;
import com.xcode.mybatis.mapper.insert.InsertSelectiveMapper;
import com.xcode.mybatis.mapper.select.SelectByExampleMapper;
import com.xcode.mybatis.mapper.select.SelectByPrimaryKeyMapper;
import com.xcode.mybatis.mapper.update.UpdateByPrimaryKeySelectiveMapper;


public interface SimpleMapper<TRecord,TKey>
    extends DeleteByPrimaryKeyMapper<TKey>
        ,InsertSelectiveMapper<TRecord>
        ,UpdateByPrimaryKeySelectiveMapper<TRecord>
        ,SelectByPrimaryKeyMapper<TKey>
{

}
