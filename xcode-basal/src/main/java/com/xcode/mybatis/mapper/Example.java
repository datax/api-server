package com.xcode.mybatis.mapper;


import java.util.ArrayList;
import java.util.List;

public final class Example {
    public static class Criterion {
        private String condition;
        private Object value;
        private Object secondValue;
        private boolean noValue;
        private boolean singleValue;
        private boolean betweenValue;
        private boolean listValue;
        private String typeHandler;

        public String getCondition() {
            return condition;
        }
        public Object getValue() {
            return value;
        }
        public Object getSecondValue() {
            return secondValue;
        }
        public boolean isNoValue() {
            return noValue;
        }
        public boolean isSingleValue() {
            return singleValue;
        }
        public boolean isBetweenValue() {
            return betweenValue;
        }
        public boolean isListValue() {
            return listValue;
        }
        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }
        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }
        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }
        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
    public static class Criteria {
        protected java.util.List<Criterion> criteria;
        protected Criteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }
        public boolean isValid() {
            return criteria.size() > 0;
        }
        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }
        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }
        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria isNull(String field) {
            addCriterion(String.format("%s is null",field));
            return (Criteria) this;
        }
        public Criteria notNull(String field) {
            addCriterion(String.format("%s is not null",field));
            return (Criteria) this;
        }
        public Criteria eq(String field,Object value) {
            addCriterion( String.format("%s =",field), value, field);
            return (Criteria) this;
        }
        public Criteria ne(String field,Object value) {
            addCriterion( String.format("%s <>",field), value, field);
            return (Criteria) this;
        }
        public Criteria gt(String field,Object value) {
            addCriterion( String.format("%s >",field), value, field);
            return (Criteria) this;
        }
        public Criteria gte(String field,Object value) {
            addCriterion( String.format("%s >=",field), value, field);
            return (Criteria) this;
        }
        public Criteria lt(String field,Object value) {
            addCriterion( String.format("%s <",field), value, field);
            return (Criteria) this;
        }
        public Criteria lte(String field,Object value) {
            addCriterion( String.format("%s <=",field), value, field);
            return (Criteria) this;
        }
        public Criteria between(String field,Object value1,Object value2) {
            addCriterion( String.format("%s between",field), value1 ,value2 , field);
            return (Criteria) this;
        }
        public Criteria notBetween(String field,Object value1,Object value2) {
            addCriterion( String.format("%s not between",field), value1 ,value2 , field);
            return (Criteria) this;
        }
        public Criteria like(String field,String value) {
            addCriterion(String.format("%s like",field),value,field);
            return (Criteria) this;
        }
        public Criteria notLike(String field,String value) {
            addCriterion(String.format("%s not like",field),value,field);
            return (Criteria) this;
        }
    }

    protected String orderByClause;
    protected boolean distinct;
    protected java.util.List<Criteria> oredCriteria;

    public Example() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }
    public String getOrderByClause() {
        return orderByClause;
    }
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }
    public boolean isDistinct() {
        return distinct;
    }
    public java.util.List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }
}
