package com.xcode.mybatis.mapper.select;

import com.xcode.mybatis.mapper.Example;

import java.util.List;

/**
 * Created by Administrator on 2016/4/4.
 */
public interface SelectByExampleWithBLOBsMapper {
    <TRecord> List<TRecord> selectByExampleWithBLOBs(Example example);
}
