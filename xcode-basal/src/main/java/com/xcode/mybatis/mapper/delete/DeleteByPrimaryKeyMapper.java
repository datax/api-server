package com.xcode.mybatis.mapper.delete;


public interface DeleteByPrimaryKeyMapper<TKey> {

    int deleteByPrimaryKey(TKey id);
}
