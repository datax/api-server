package com.xcode.mybatis.mapper.select;

import com.xcode.mybatis.mapper.Example;

import java.util.List;

/**
 * Created by Administrator on 2016/4/4.
 */
public interface SelectByExampleMapper {
    <TRecord> List<TRecord> selectByExample(Example example);
}
