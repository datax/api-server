package com.xcode.mybatis.mapper.delete;

import com.xcode.mybatis.mapper.Example;

/**
 * Created by Administrator on 2016/4/4.
 */
public interface DeleteByExampleMapper {
    int deleteByExampleMapper(Example example);
}
