package com.xcode.mybatis.mapper.select;

/**
 * Created by Administrator on 2016/4/4.
 */
public interface SelectByPrimaryKeyMapper<TKey> {
    <TRecord> TRecord selectByPrimaryKey(TKey key);
}
