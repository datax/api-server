package com.xcode.mybatis.mapper.insert;


public interface InsertSelectiveMapper<TRecord> {
    int insertSelective(TRecord record);
}
