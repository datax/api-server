package com.xcode.mybatis.mapper.update;

/**
 * Created by Administrator on 2016/4/4.
 */
public interface UpdateByPrimaryKeySelectiveMapper<TRecord> {
    int updateByPrimaryKeySelective(TRecord record);
}
