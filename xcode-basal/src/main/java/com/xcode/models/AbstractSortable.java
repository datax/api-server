package com.xcode.models;

/**
 * Created by Administrator on 2016/5/11.
 */
public  abstract class AbstractSortable<TKey> extends AbstractDeletable<TKey> {
    private Integer ordinal;

    public Integer getOrdinal() {
        return ordinal;
    }

    public void setOrdinal(Integer ordinal) {
        this.ordinal = ordinal;
    }
}
