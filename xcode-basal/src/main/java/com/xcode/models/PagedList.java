package com.xcode.models;



import java.util.List;

/**
 * Created by Administrator on 2016/4/26.
 */
public class PagedList<T> implements IPagedList<T> {
    private List<T> list;

    public List<T> getList() {
        return list;
    }
    private int total;
    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
