package com.xcode.models;

import java.util.List;

public interface IPagedList<T> {
    List<T> getList();
    int getTotal();
}
