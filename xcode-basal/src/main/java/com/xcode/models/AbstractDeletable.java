package com.xcode.models;

/**
 * Created by Administrator on 2016/5/11.
 */
public abstract class AbstractDeletable<TKey> {
    private TKey id;

    //是否已删除，性对系统而言，对用户而言不存在
    private Boolean deleted;

    //是否隐藏，系统标志，对用户而言不存在
    private Boolean hidden;



    public TKey getId() {
        return id;
    }

    public void setId(TKey id) {
        this.id = id;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }
}
