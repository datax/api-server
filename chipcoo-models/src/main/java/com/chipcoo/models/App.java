package com.chipcoo.models;

/**
 * Created by Administrator on 2016/3/25.
 */
public interface App {
    int APP=1;
    int IE=2;
    int CHROME=3;
    int SAFARI=4;
    int FIREFOX=5;
    int OPERA=6;

    static IdentifyVersion parseUserAgent(String userAgent){
        if(null==userAgent||userAgent.length()==0) return null;
        userAgent = userAgent.toLowerCase();
        IdentifyVersion ret = null;
        int iStart;
        if ( (iStart=userAgent.indexOf("app/"))==0) {
            ret = new IdentifyVersion(APP,userAgent,iStart,"app/");
        }
        else if ( (iStart=userAgent.indexOf("chrome/")) >= 0 ) {
            ret = new IdentifyVersion(CHROME,userAgent,iStart,"chrome/");
        }
        else if (userAgent.indexOf("safari/") >= 0) {
            ret = new IdentifyVersion(SAFARI,userAgent,iStart,"safari/");
        } else if (userAgent.indexOf("opera/") >= 0) {
            ret = new IdentifyVersion(OPERA,userAgent,iStart,"opera/");
        } else if (userAgent.indexOf("firefox/") >= 0) {
            ret = new IdentifyVersion(FIREFOX,userAgent,iStart,"firefox/");
        } else if (userAgent.indexOf("msie ") >= 0) {
            ret = new IdentifyVersion(IE,userAgent,iStart,"msie ");
        }
        return ret;
    }

}
