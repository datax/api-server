package com.chipcoo.models.gen;

import java.util.Date;

public class Finger {
    private String id;

    private Date createdAt;

    private Date updatedAt;

    private Integer os;

    private Float osVersion;

    private Integer app;

    private Float appVersion;

    private Integer appFaker;

    private Integer botFaker;

    private String model;

    private Date lastAccessed;

    private String lastAction;

    private Integer errorCount;

    private String jsonSchema;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Integer getOs() {
        return os;
    }

    public void setOs(Integer os) {
        this.os = os;
    }

    public Float getOsVersion() {
        return osVersion;
    }

    public void setOsVersion(Float osVersion) {
        this.osVersion = osVersion;
    }

    public Integer getApp() {
        return app;
    }

    public void setApp(Integer app) {
        this.app = app;
    }

    public Float getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(Float appVersion) {
        this.appVersion = appVersion;
    }

    public Integer getAppFaker() {
        return appFaker;
    }

    public void setAppFaker(Integer appFaker) {
        this.appFaker = appFaker;
    }

    public Integer getBotFaker() {
        return botFaker;
    }

    public void setBotFaker(Integer botFaker) {
        this.botFaker = botFaker;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model == null ? null : model.trim();
    }

    public Date getLastAccessed() {
        return lastAccessed;
    }

    public void setLastAccessed(Date lastAccessed) {
        this.lastAccessed = lastAccessed;
    }

    public String getLastAction() {
        return lastAction;
    }

    public void setLastAction(String lastAction) {
        this.lastAction = lastAction == null ? null : lastAction.trim();
    }

    public Integer getErrorCount() {
        return errorCount;
    }

    public void setErrorCount(Integer errorCount) {
        this.errorCount = errorCount;
    }

    public String getJsonSchema() {
        return jsonSchema;
    }

    public void setJsonSchema(String jsonSchema) {
        this.jsonSchema = jsonSchema == null ? null : jsonSchema.trim();
    }
}