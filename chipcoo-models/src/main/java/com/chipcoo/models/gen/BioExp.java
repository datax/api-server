package com.chipcoo.models.gen;

import com.xcode.models.AbstractSortable;
import java.util.Date;

public class BioExp extends AbstractSortable<Integer> {
    private Integer userID;

    private Date startAt;

    private Date entAt;

    private String projectName;

    private String gain;

    private String major;

    private Integer numOfPeople;

    private String job;

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEntAt() {
        return entAt;
    }

    public void setEntAt(Date entAt) {
        this.entAt = entAt;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName == null ? null : projectName.trim();
    }

    public String getGain() {
        return gain;
    }

    public void setGain(String gain) {
        this.gain = gain == null ? null : gain.trim();
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major == null ? null : major.trim();
    }

    public Integer getNumOfPeople() {
        return numOfPeople;
    }

    public void setNumOfPeople(Integer numOfPeople) {
        this.numOfPeople = numOfPeople;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job == null ? null : job.trim();
    }
}