package com.chipcoo.models.gen;

public class ConfSiteMatcher {
    private Integer siteID;

    private Boolean botOnly;

    private String hosts;

    private String appName;

    public Integer getSiteID() {
        return siteID;
    }

    public void setSiteID(Integer siteID) {
        this.siteID = siteID;
    }

    public Boolean getBotOnly() {
        return botOnly;
    }

    public void setBotOnly(Boolean botOnly) {
        this.botOnly = botOnly;
    }

    public String getHosts() {
        return hosts;
    }

    public void setHosts(String hosts) {
        this.hosts = hosts == null ? null : hosts.trim();
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName == null ? null : appName.trim();
    }
}