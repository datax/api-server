package com.chipcoo.models.gen;

public class DicLoc extends DicLocKey {
    private String zh_cn_name;

    private String zh_cn_pinyin;

    private String zh_cn_pinyinIndex;

    private String en_name;

    public String getZh_cn_name() {
        return zh_cn_name;
    }

    public void setZh_cn_name(String zh_cn_name) {
        this.zh_cn_name = zh_cn_name == null ? null : zh_cn_name.trim();
    }

    public String getZh_cn_pinyin() {
        return zh_cn_pinyin;
    }

    public void setZh_cn_pinyin(String zh_cn_pinyin) {
        this.zh_cn_pinyin = zh_cn_pinyin == null ? null : zh_cn_pinyin.trim();
    }

    public String getZh_cn_pinyinIndex() {
        return zh_cn_pinyinIndex;
    }

    public void setZh_cn_pinyinIndex(String zh_cn_pinyinIndex) {
        this.zh_cn_pinyinIndex = zh_cn_pinyinIndex == null ? null : zh_cn_pinyinIndex.trim();
    }

    public String getEn_name() {
        return en_name;
    }

    public void setEn_name(String en_name) {
        this.en_name = en_name == null ? null : en_name.trim();
    }
}