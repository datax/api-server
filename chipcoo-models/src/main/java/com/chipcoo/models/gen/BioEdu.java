package com.chipcoo.models.gen;

import com.xcode.models.AbstractSortable;
import java.util.Date;

public class BioEdu extends AbstractSortable<Integer> {
    private Integer userID;

    private Date startAt;

    private Date entAt;

    private String college;

    private String major;

    private String degree;

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public Date getStartAt() {
        return startAt;
    }

    public void setStartAt(Date startAt) {
        this.startAt = startAt;
    }

    public Date getEntAt() {
        return entAt;
    }

    public void setEntAt(Date entAt) {
        this.entAt = entAt;
    }

    public String getCollege() {
        return college;
    }

    public void setCollege(String college) {
        this.college = college == null ? null : college.trim();
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major == null ? null : major.trim();
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree == null ? null : degree.trim();
    }
}