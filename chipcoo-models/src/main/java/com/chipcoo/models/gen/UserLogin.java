package com.chipcoo.models.gen;

import java.util.Date;

public class UserLogin {
    private Integer id;

    private Date createdAt;

    private String serverHost;

    private Long clientIP;

    private Integer userID;

    private String fingerID;

    private Integer passportID;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public String getServerHost() {
        return serverHost;
    }

    public void setServerHost(String serverHost) {
        this.serverHost = serverHost == null ? null : serverHost.trim();
    }

    public Long getClientIP() {
        return clientIP;
    }

    public void setClientIP(Long clientIP) {
        this.clientIP = clientIP;
    }

    public Integer getUserID() {
        return userID;
    }

    public void setUserID(Integer userID) {
        this.userID = userID;
    }

    public String getFingerID() {
        return fingerID;
    }

    public void setFingerID(String fingerID) {
        this.fingerID = fingerID == null ? null : fingerID.trim();
    }

    public Integer getPassportID() {
        return passportID;
    }

    public void setPassportID(Integer passportID) {
        this.passportID = passportID;
    }
}