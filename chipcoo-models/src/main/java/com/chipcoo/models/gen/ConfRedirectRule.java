package com.chipcoo.models.gen;

public class ConfRedirectRule {
    private Integer id;

    private String cookieName;

    private Integer cookieAge;

    private String urlRedirect;

    private Boolean disabled;

    private String matchSchema;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCookieName() {
        return cookieName;
    }

    public void setCookieName(String cookieName) {
        this.cookieName = cookieName == null ? null : cookieName.trim();
    }

    public Integer getCookieAge() {
        return cookieAge;
    }

    public void setCookieAge(Integer cookieAge) {
        this.cookieAge = cookieAge;
    }

    public String getUrlRedirect() {
        return urlRedirect;
    }

    public void setUrlRedirect(String urlRedirect) {
        this.urlRedirect = urlRedirect == null ? null : urlRedirect.trim();
    }

    public Boolean getDisabled() {
        return disabled;
    }

    public void setDisabled(Boolean disabled) {
        this.disabled = disabled;
    }

    public String getMatchSchema() {
        return matchSchema;
    }

    public void setMatchSchema(String matchSchema) {
        this.matchSchema = matchSchema == null ? null : matchSchema.trim();
    }
}