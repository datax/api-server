package com.chipcoo.models;

/**
 * Created by Administrator on 2016/4/2.
 */
public interface Bot {
    int BAI_DU=1;
    int GOOGLE=2;
    int _360=3;  //360Spider
    int SO_GOU=4;
    int BING =5;
    int YAHOO =6;
    int IASK = 7;
    int YO_DAO=8;
    int MSN = 9;
    int AHREFS=10;
    int UNKNOWN=255;

    static int parseUserAgent(String userAgent){
        if(null==userAgent||userAgent.length()==0) return 0;
        userAgent = userAgent.toLowerCase();
        if(userAgent.indexOf("baiduspider")>=0) return Bot.BAI_DU;
        if(userAgent.indexOf("googlebot")>=0) return Bot.GOOGLE;
        if(userAgent.indexOf("360spider")>=0) return Bot._360;
        if(userAgent.indexOf("sogou web spider")>=0) return Bot.SO_GOU;
        if(userAgent.indexOf("bingbot")>=0) return Bot.BING;
        if(userAgent.indexOf("yahoo! slurp")>=0) return Bot.YAHOO;
        if(userAgent.indexOf("iaskspider")>=0) return Bot.IASK;
        if(userAgent.indexOf("yodaobot")>=0) return Bot.YO_DAO;
        if(userAgent.indexOf("msnbot")>=0) return Bot.MSN;
        if(userAgent.indexOf("ahrefsbot")>=0) return Bot.AHREFS;

        if(userAgent.indexOf("bot")>=0||userAgent.indexOf("spider")>=0) return  Bot.UNKNOWN;

        return 0;
    }
}
