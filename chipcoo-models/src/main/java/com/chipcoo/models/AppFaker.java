package com.chipcoo.models;



/**
 * Created by Administrator on 2016/3/25.
 */
public interface AppFaker {
    int QQ=1;
    int _360=2;
    int SOGOU=3;
    int UC=4;
    int MAXTHON=6;
    int THEWORLD=7;


    static int parseUserAgent(String text){
        if(null==text||text.length()==0) return 0;
        text = text.toLowerCase();
        if(text.indexOf("tencenttraveler")>=0|| text.indexOf("qqbrowser")>=0) return QQ;
        else if(text.indexOf("360se")>=0|| text.indexOf("360ee")>=0) return _360;
        else if(text.indexOf("ucweb")>=0) return UC;
        else if(text.indexOf("maxthon")>=0) return MAXTHON;
        else if(text.indexOf("the world")>=0) return THEWORLD;
        else if (text.matches(".*se\\s+\\d+(\\.[^\\s]+)*\\s+metasr\\s+\\d+(\\.[^\\s]+)*.*")) return SOGOU;

        return 0;
    }
}
