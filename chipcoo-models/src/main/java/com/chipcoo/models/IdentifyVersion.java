package com.chipcoo.models;

/**
 * Created by Administrator on 2016/4/2.
 */
public class IdentifyVersion {
    static String findWithCharacter(String source,String prefix,int iStart,String... characterString ){
        iStart+=prefix.length();
        int iEnd = -1;
        for(int i=0;i<characterString.length;i++)
        {
            if(characterString[i]== " "){
                while ( iStart+1<source.length() && source.charAt(iStart+1)==' ') iStart++;

                iEnd = source.indexOf(" ",iStart);

            }
            else iEnd = source.indexOf(characterString[i],iStart);

            if(iEnd>iStart) break;
        }
        if(iEnd>iStart) return  source.substring(iStart,iEnd).trim().replace("_",".");
        else return source.substring(iStart).trim().replace("_",".");
    }
    private static void trySetVersion(String userAgent,String prefix,int iStart,IdentifyVersion finger){
        String szVersion = findWithCharacter(userAgent,prefix,iStart,";",")"," ");
        try{finger.setVersion(Float.parseFloat(szVersion));}
        catch (Exception ex){}
    }
    int id;
    float version;
    IdentifyVersion(int id){
        this.id=id;
    }
    IdentifyVersion(int id,String userAgent,int iStart,String prefix){
        this(id);
        trySetVersion(userAgent,prefix,iStart,this);
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getVersion() {
        return version;
    }

    public void setVersion(float version) {
        this.version = version;
    }
}
