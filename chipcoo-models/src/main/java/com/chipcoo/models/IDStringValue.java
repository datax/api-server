package com.chipcoo.models;

/**
 * Created by Administrator on 2016/3/31.
 */
public class IDStringValue<TID> extends IDValue<TID,String> {
    @Override
    public void setValue(String s) {
        super.setValue(null==s?null:s.toString());
    }
}
