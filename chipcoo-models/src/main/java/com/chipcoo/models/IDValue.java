package com.chipcoo.models;




public class IDValue<TID,TValue> {
    private TID id;
    private TValue value;

    public TID getId() {
        return id;
    }

    public void setId(TID id) {
        this.id = id;
    }

    public TValue getValue() {
        return value;
    }

    public void setValue(TValue value) {
        this.value = value;
    }
}
