package com.chipcoo.models;


public interface OS {
    int WIN=1;
    int OSX=2;
    int LINUX=3;
    int IOS =4;
    int ANDROID=5;
    int WINPHONE=6;
    int FREEBSD=7;

    static IdentifyVersion parseUserAgent(String userAgent){
        if(null==userAgent||userAgent.length()==0) return null;
        userAgent = userAgent.toLowerCase();
        IdentifyVersion ret = null;
        int iStart;
        if(  (iStart=userAgent.indexOf("android"))>=0 ){
            ret = new IdentifyVersion(ANDROID,userAgent,iStart,"android");
        }

        //region ios
        else  if(  (iStart=userAgent.indexOf("ios"))>=0 ){
            ret = new IdentifyVersion(IOS,userAgent,iStart,"ios");
        }else  if(  (iStart=userAgent.indexOf("iphone os"))>=0 ){
            ret = new IdentifyVersion(IOS,userAgent,iStart,"iphone os");
        }else  if(  (iStart=userAgent.indexOf("cpu os"))>=0 ){
            ret = new IdentifyVersion(IOS,userAgent,iStart,"cpu os");
        }
        //endregion

        //region windows phone
        else  if(  (iStart=userAgent.indexOf("windows phone os"))>=0 ){
            ret = new IdentifyVersion(IOS,userAgent,iStart,"windows phone os");
        }
        //endregion

        //region PC OS:win osx linux
        //region win
        else  if(  (iStart=userAgent.indexOf("windows nt"))>=0 ){
            ret = new IdentifyVersion(WIN,userAgent,iStart,"windows nt");
        }else  if(  (iStart=userAgent.indexOf("windows"))>=0 ){
            ret = new IdentifyVersion(WIN);
            String szVersion = IdentifyVersion.findWithCharacter(userAgent,"windows",iStart,";",")"," ");
            if(szVersion.equals("me")) ret.setVersion(4.0f);
            else if(szVersion.equals("98")) ret.setVersion(4.1f);
        }
        //endregion
        //region mac os
        else  if(  (iStart=userAgent.indexOf("mac os x"))>=0 ){
            ret = new IdentifyVersion(OSX,userAgent,iStart,"mac os x");
        } else  if(  (iStart=userAgent.indexOf("freebsd"))>=0 ) {
            ret = new IdentifyVersion(FREEBSD);
        }
        //endregion
        else  if(  (iStart=userAgent.indexOf("linux"))>=0 ) {
            ret = new IdentifyVersion(LINUX);
        }
        //endregion
        //endregion

        return ret;
    }
}
