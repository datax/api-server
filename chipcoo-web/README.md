#API约定 `V 0.0.1`

## WEB API 会话约定
***

客户端（WEB AJAX、APP REST）通过`HTTP请求`（G-GET,P-POST,D-DELETE,PUT-PUT）请求服务器特定资源，来获取相应信息或执行相关动作

**客户端-请求动作默认约定**

* **G** 动作，通常用于获取服务端信息
* **P** 动作，通常用于告之服务端我需要添加新的数据或执行某个动作
* **D** 动作，通常用于告之服务端我需要删除某个数据或资源
* **PUT** 动作，通常用于告之服务端我需要更新某个数据或资源

**服务端-应答或回传约定**

**数据格式(json)**
>{
     code:(int),
     message:(string),
     data:(var)
}

* code 必提供，`当code==0`，表示当前请求是得到正确的执行或返回
* message 可能不提供，`当code==-99`是，必然存在，它是对错误信息的补充解释
* data    可能不提供，`当code==0`，可能存在或不存在，当存在时，其为客户端的Model，客户端可能需要根据Model针对具体的业务做界面渲染；`当code==100`时，必然存在，是对错误代码的补充

**code 码 详细列表**

* 1;       //需要提交finger

* 2;       //检测到客户不支持cookie

* 3;       //需要验证码

* 4;       //未找到

* 5:       //操作频繁或太快

* 100;    //组合错误，错误详情参考DATA，例如：登录时,{code:100,data:{password:104}} 表示 密码错误

* 101;    //验证码错误

* 102;    //格式错误

* 103;    //实体存在，例如账号

* 104;    //值无效

* -1;     //应用错误

* -99;    //自定义字符串消息

##业务会话 约定
***

**文档格式说明**
> ACTION PATH   
> DATA_FORMAT REQUEST_DATA    
> ==>SUCCEED_DATA

* **ACTION**          指HTTP请求方式，已知的请求方式为：（G-GET,P-POST,D-DELETE,PUT-PUT）

* **PATH**            服务端资源路径，以"/"开头

* **DATA_FORMAT**      相应请求，应随同提交的数据格式，已知数据格式为：( **Q** -path query 即URL参数请求、 **F**-application/x-www-form-urlencoded，即FORM表单 **B**-multipart/form-data; boundary，通常用于附件上传、 **FJ**HTTP REQUEST-BODY，为JSON字符串）

* **REQUEST_DATA**   K:VTYPE(字段说明)集合，相应请求，客户端应提交给服务端的数据，如：username:int(用户名)

* **SUCCEED_DATA**   K:VTYPE(字段说明)集合，相应请求成功，并且有返回Model时，对data字段的填充

**REQUEST_DATA SUCCEED_DATA 字段基础类型（VTYPE)说明**

* **int**        整形
* **bool**       boolean
* **string**     字符串
* **var**        可变不限任意对象

***  

* **用户登录之前**
G /passport/login
==>username:string(账号，邮箱或手机),gt:string,challenge:string
如果 challenge 存在 且 不为空 ，这应显示验证码
* **用户登录提交**  
P /passport/login  
F username:string(账号，邮箱或手机),password:string(密码),geetest_challenge:string,geetest_validate:string,geetest_seccode:string
==>

* **用户注册之前**
G /passport/join
==> gt:string,challenge:string
如果 challenge 存在 且 不为空 ，这应显示验证码
* **用户注册提交 获取二级验证码**
P /passport/join
F username:string(账号，邮箱或手机),password:string(密码),geetest_challenge:string,geetest_validate:string,geetest_seccode:string
==>
* **用户提交注册**
P /passport/joindone
F username:string(账号，邮箱或手机),password:string(密码),code:string(短信验证码)
==>


* **忘记密码之前**
G /passport/forgot
==> gt:string,challenge:string
如果 challenge 存在 且 不为空 ，这应显示验证码
* **用户提交  获取二级验证码**
P /passport/forgot
F username:string(账号，邮箱或手机),geetest_challenge:string,geetest_validate:string,geetest_seccode:string
==>
* **用户提交注册**
P /passport/forgotdone
F username:string(账号，邮箱或手机),password:string(新密码),code:string(短信验证码)
==>

## WEBSOCKET API 会话约定
***

**文档格式说明**
> S->C CODE DATA_FORMAT DATA  或者  
  C->S CODE DATA_FORMAT DATA  
  
**实际传输格式**  
> CODEDATA  

* **S** 指服务端
* **C** 指客户/应用端
* **->** 消息流向，S->C即，服务端主动PUSH消息至客户端，C->S，即客户端发送消息至服务端
* **CODE** 指令码(int)，服务端或客户端根据约定的指令码执行相关服务端或客户端动作
* **DATA** 跟指令相关的数据，可能为空，数据表达形式参考 **DATA_FORMAT**
  
##S->C 服务端指令码
***

##C->S 客户端指令码
***