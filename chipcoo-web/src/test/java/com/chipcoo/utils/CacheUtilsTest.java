package com.chipcoo.utils;

import com.chipcoo.AbstractSpringMVCTest;
import org.junit.Test;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;


public class CacheUtilsTest extends AbstractSpringMVCTest {
    @Test
    public void getManager(){
        CacheManager manager = CacheUtils.getManager();
        Cache cache = manager.getCache("tb_basal_loc");
        System.out.println(manager);
        System.out.println(cache);
    }
}
