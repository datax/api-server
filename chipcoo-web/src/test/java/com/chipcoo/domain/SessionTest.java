package com.chipcoo.domain;

import com.chipcoo.AbstractSpringMVCTest;
import org.junit.Test;

import javax.servlet.http.Cookie;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Administrator on 2016/5/8.
 */
public class SessionTest extends AbstractSpringMVCTest {

    @Test
    public void retrievalSessionWithId(){
        try {
            System.out.println(getJSON(getMockMvc().perform(
                getMethod("/passport/login",false)
                    .cookie( new Cookie("_sid","cd78f912ce6740444bc499cd19420389"))
            ).andExpect(status().isOk()).andReturn()
            ));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
