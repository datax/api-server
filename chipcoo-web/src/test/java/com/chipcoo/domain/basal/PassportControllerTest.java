package com.chipcoo.domain.basal;

import com.chipcoo.AbstractSpringMVCTest;
import org.junit.Test;
import org.springframework.test.web.servlet.MvcResult;

import javax.servlet.http.Cookie;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Administrator on 2016/5/8.
 */
public class PassportControllerTest extends AbstractSpringMVCTest {

    @Test
    public void loginGetTest(){
        try {
            String json = getJSON("/passport/login");
            System.out.println(json);
            json =  getJSON("/passport/fingerprint");
            System.out.println(json);
            MvcResult mvcResult = getMockMvc().perform(
                    postMethod("/passport/fingerprint")
                            .param("finger","6e943e336ba81800edfd9cda2c07416c")
                            .param("jsonSchema","{\"ua\":\"Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.106 Safari/537.36\",\"lang\":\"zh-CN\",\"color\":24,\"size\":[1920,1080],\"vsize\":[1920,1036],\"time\":-480,\"st\":1,\"ls\":1,\"lied_lang\":0,\"lied_size\":0,\"lied_os\":0,\"lied_browser\":0}")
                            .cookie(new Cookie("cookie_enable","1")
                            )
            ).andExpect(status().isOk()).andReturn();
            json = getJSON(mvcResult);
            System.out.println(json);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
