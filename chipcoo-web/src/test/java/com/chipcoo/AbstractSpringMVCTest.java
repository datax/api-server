package com.chipcoo;

import com.alibaba.fastjson.JSON;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.ModelAndView;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@ContextConfiguration(name = "parent", locations = "file:src/main/webapp/WEB-INF/spring-app-test.xml")
public abstract class AbstractSpringMVCTest {
    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;
    protected MockMvc getMockMvc() {
        return mockMvc;
    }


    @Before
    public final void setup(){


        // webAppContextSetup 注意上面的static import
        // webAppContextSetup 构造的WEB容器可以添加fileter 但是不能添加listenCLASS
        // WebApplicationContext context =  ContextLoader.getCurrentWebApplicationContext();
        // 如果控制器包含如上方法 则会报空指针
        DelegatingFilterProxy filterProxy = new DelegatingFilterProxy();
        filterProxy.setTargetBeanName("shiroFilter");
        filterProxy.setServletContext(wac.getServletContext());

        this.mockMvc = webAppContextSetup(this.wac)
                 .addFilter(new CharacterEncodingFilter(),"/*")
                 .addFilter(filterProxy,"/*")
                 .build();

        //region fixed shiro
        ShiroTestHelper.bindSubject(null);
        //endregion
    }

    protected MockHttpServletRequestBuilder getMethod(String url,boolean isHtml){
        if(isHtml) return buildBrowserHeaders(get(url));
        return buildHeaders(get(url));
    }
    private MockHttpServletRequestBuilder buildHeaders(MockHttpServletRequestBuilder builder){
        return builder.accept(MediaType.APPLICATION_JSON_UTF8);
    }
    private MockHttpServletRequestBuilder buildBrowserHeaders(MockHttpServletRequestBuilder builder){
        return builder.accept(MediaType.TEXT_HTML);
    }
    protected String getHTMl(String url) throws Exception{
        MvcResult mvcResult = getMockMvc().perform(getMethod(url,true)).andExpect(status().isOk()).andReturn();
        return getJSON(mvcResult);
    }
    protected String getJSON(String url) throws Exception{
        MvcResult mvcResult = getMockMvc().perform(getMethod(url,false)).andExpect(status().isOk()).andReturn();
        return getJSON(mvcResult);
    }
    protected String getJSON(MvcResult mvcResult) throws  Exception{
        ModelAndView mv =mvcResult.getModelAndView();
        if(null!=mv) {
            return JSON.toJSONString(mvcResult.getModelAndView().getModel().get("responseResult"));
        }
        return mvcResult.getResponse().getContentAsString();
    }

    protected MockHttpServletRequestBuilder postMethod(String url )  throws  Exception{
        MockHttpServletRequestBuilder builder =  buildHeaders(post(url));

        return builder;

    }
}
