package com.chipcoo.web.utils;

import com.chipcoo.ResponseResult;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

public final class ViewUtils {
    public static View NOT_ACCEPTABLE_VIEW = new View() {

        @Override
        public String getContentType() {
            return null;
        }

        @Override
        public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response) {
            response.setStatus(HttpServletResponse.SC_NOT_ACCEPTABLE);
        }

    };

    public static View redirectView(String url) {
        return new RedirectView(url);
    }

    /*public static Object bestView(ResponseResult result){
        if(null!=result){
            if(!MediaTypeUtils.isAjax()){
                Object o;
                if( null!=( o = result.get("redirect") )) {
                    return redirectView(WebUtils.getURL(result,));
                }
            }
        }
        return result;
    }*/
}
