package com.chipcoo.web.utils;


import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * 单元测试需要....
 */
public final class ApplicationContextHolder implements ApplicationContextAware,DisposableBean {
    private static ApplicationContext applicationContext;
    public static ApplicationContext getContext(){
        return ApplicationContextHolder.applicationContext;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ApplicationContextHolder.applicationContext = applicationContext;
    }

    @Override
    public void destroy() throws Exception {
        if(null!=ApplicationContextHolder.applicationContext)
            ApplicationContextHolder.applicationContext = null;
    }
}
