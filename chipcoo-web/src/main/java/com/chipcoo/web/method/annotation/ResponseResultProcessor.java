package com.chipcoo.web.method.annotation;

import com.chipcoo.ResponseResult;
import org.springframework.core.MethodParameter;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.ModelAndViewContainer;

public class ResponseResultProcessor implements HandlerMethodReturnValueHandler {
    @Override
    public void handleReturnValue(Object returnValue, MethodParameter returnType, ModelAndViewContainer mavContainer, NativeWebRequest webRequest) throws Exception {
        mavContainer.addAttribute("responseResult",returnValue);
    }

    @Override
    public boolean supportsReturnType(MethodParameter returnType) {
        //如果继承于ResponseResult
        return ResponseResult.class.isAssignableFrom(returnType.getParameterType());
    }


}
