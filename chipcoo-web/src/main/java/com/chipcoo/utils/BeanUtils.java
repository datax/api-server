package com.chipcoo.utils;

import com.chipcoo.web.utils.WebUtils;

/**
 * Created by Administrator on 2016/5/6.
 */
public final class BeanUtils {
    public static Object getServletBean(String bean){
        try {
            return WebUtils.getServletApplicationContext(WebUtils.getWebApplicationContext().getServletContext()).getBean(bean);
        }catch (Exception ex){
            ex.printStackTrace();
            return  null;
        }
    }
    @SuppressWarnings("unchecked")
    public static <T> T getServletBean(String bean,Class<T> beanType) {
        try {
            return WebUtils.getServletApplicationContext(WebUtils.getWebApplicationContext().getServletContext()).getBean(bean,beanType);
        }catch (Exception ex){
            ex.printStackTrace();
            return  null;
        }
    }
    public static Object getBean(String bean){
        try {
            return WebUtils.getWebApplicationContext().getBean(bean);
        }catch (Exception ex){
            ex.printStackTrace();
            return  null;
        }
    }
    @SuppressWarnings("unchecked")
    public static <T> T getBean(String bean,Class<T> beanType) {
        try {
            return WebUtils.getWebApplicationContext().getBean(bean,beanType);
        }catch (Exception ex){

            ex.printStackTrace();
            return  null;
        }
    }
}
