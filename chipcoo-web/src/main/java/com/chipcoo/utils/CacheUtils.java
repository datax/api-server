package com.chipcoo.utils;

import org.springframework.cache.CacheManager;

/**
 * Created by Administrator on 2016/5/7.
 */
public final class CacheUtils {
    public static CacheManager getManager(){
        return BeanUtils.getBean("cacheManager",CacheManager.class);
    }

}
