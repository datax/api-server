package com.chipcoo.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.chipcoo.captcha.geetest.GeetestLib;
import org.apache.shiro.session.Session;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.util.PathMatcher;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import java.util.regex.Pattern;

import static com.chipcoo.web.utils.WebUtils.getCurrentRequest;

public final class ValidateUtils {

    /**
     * 判断当前请求是否需要验证码
     * @param req
     * @return
     */
    public static boolean isCaptchaEnabled(HttpServletRequest req) {
        Object oEnabled = getCaptchaChallenge(req);
        if (null != oEnabled) return true;

        boolean enable;
        if (BeanUtils.getBean("pathMatching", PathMatcher.class).match("/passport/login", WebUtils.getPathWithinApplication(req))) {
            enable = SecurityUtils.getSessionError() >= 5;
        }
        else enable = true;

        if (enable) {
            //always rebuild challenge (one challenge one draw)
            return createCaptchaChallenge(req);
        }

        return enable;
    }

    private static String KEY_CAPTCHA_ERROR = "CAPTCHA_ERROR";
    public static boolean isCaptchaValidated(ServletRequest req){
        return null==req.getAttribute(KEY_CAPTCHA_ERROR);
    }
    public static boolean validateRequest(ServletRequest req){
        if(null!=getCaptchaChallenge(req)) {
            String challenge = com.chipcoo.web.utils.WebUtils.getString(req, GeetestLib.fn_geetest_challenge);
            String validate = com.chipcoo.web.utils.WebUtils.getString(req, GeetestLib.fn_geetest_validate);
            String seccode = com.chipcoo.web.utils.WebUtils.getString(req, GeetestLib.fn_geetest_seccode);

            GeetestLib geetestLib = new GeetestLib();
            boolean isOk = geetestLib.enhencedValidateRequest(challenge, validate, seccode).equals(GeetestLib.success_res);
            if (!isOk) {
                req.setAttribute(KEY_CAPTCHA_ERROR, true);
            }
            return isOk;
        }

        return true;
    }

    private static String KEY_CAPTCHA_CHALLENGE = "CAPTCHA_CHALLENGE";
    public static Object getCaptchaChallenge(){
        return getCaptchaChallenge(getCurrentRequest());
    }
    private static Object getCaptchaChallenge(ServletRequest req){
        return req.getAttribute(KEY_CAPTCHA_CHALLENGE);
    }
    private static boolean createCaptchaChallenge(HttpServletRequest req) {
        GeetestLib geetestLib = new GeetestLib();
        if (geetestLib.preProcess() == 1) {
            JSONObject jo = JSON.parseObject(geetestLib.getResponseStr());
            String challenge = jo.getString("challenge");
            req.setAttribute(KEY_CAPTCHA_CHALLENGE, challenge);
            return true;
        }
        return false;
    }



    public static boolean isPhone(String text){
        return Pattern.compile("^1[0-9]{10}$", Pattern.CASE_INSENSITIVE).matcher(text).matches();
    }

    public static boolean isEmail(String text){
        return Pattern.compile("^([a-zA-Z0-9_-])+@([a-zA-Z0-9_-])+(\\.([a-zA-Z0-9_-])+)+$", Pattern.CASE_INSENSITIVE).matcher(text).matches();
    }
}
