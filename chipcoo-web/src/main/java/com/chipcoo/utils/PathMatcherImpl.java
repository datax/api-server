package com.chipcoo.utils;

import org.springframework.util.AntPathMatcher;

import java.util.Map;

public final class PathMatcherImpl extends AntPathMatcher {
    /**
     * Create a new instance with the {@link #DEFAULT_PATH_SEPARATOR}.
     */
    public PathMatcherImpl() {
        super();
        setCaseSensitive(false);
    }

    /**
     * A convenient, alternative constructor to use with a custom path separator.
     * @param pathSeparator the path separator to use, must not be {@code null}.
     * @since 4.1
     */
    public PathMatcherImpl(String pathSeparator) {
        super();
        setCaseSensitive(false);
    }



    @Override
    protected boolean doMatch(String pattern, String path, boolean fullMatch, Map<String, String> uriTemplateVariables) {
        boolean ret =  super.doMatch(pattern, path, fullMatch, uriTemplateVariables);
        if( !ret ) {
            int iStart = path.lastIndexOf("/");

            if (path.substring(iStart > 0 ? iStart : 0).indexOf(".") < 0) {
                String newPath;
                if (path.endsWith("/")) newPath = path.concat("home");
                else newPath = path.concat("/home");

                ret = super.doMatch(pattern, newPath, fullMatch, uriTemplateVariables);
            }

            if(!ret){

            }
        }

        return  ret;
    }

    @Override
    protected AntPathStringMatcher getStringMatcher(String pattern) {
        return super.getStringMatcher(pattern);
    }
}