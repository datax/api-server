package com.chipcoo.utils;


import com.chipcoo.models.gen.DicLoc;
import com.chipcoo.services.ConfService;
import net.sf.ehcache.Ehcache;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.ehcache.EhCacheCache;

public final class LocUtils {

    private static ConfService getService(){

        return BeanUtils.getBean("confService",ConfService.class);
    }

    /**
     * 根据code path 返回完整的地理位置文字描述
     * @param codes
     * @return
     */
    public static String getLoc(int ...codes){
        StringBuilder sb = new StringBuilder();
        int[] codeArr =new int[4];
        int iZero = -1;
        for(int i=0;i<4;i++) {
            if (i < codes.length) {
                codeArr[i] = codes[i];
            } else {
                codeArr[i] = 0;
            }
            if(iZero==-1 && codeArr[i]<=0) {
                iZero = i;
                break;
            }
        }
        if( iZero == -1) iZero=4;

        while (iZero>0) {
            int[] locPath = new int[4];
            for(int i=0;i<iZero;i++) locPath[i]=codeArr[i];

            //System.out.println(ArrayUtils.toString(locPath));
            DicLoc loc = getService().getLoc(locPath[0], locPath[1], locPath[2], locPath[3]);
            if(null!=loc){
                sb.insert(0,loc.getZh_cn_name());
            }
            iZero--;
        }

        return sb.toString();
    }

    /**
     * 根据任意地理文字，返回一个DicLoc对象，如果未找到，则返回，null
     * @param loc
     * @return
     */
    public static String getPath(String loc){
        CacheManager manager = CacheUtils.getManager();
        if(null!=manager) {
            Cache cache = manager.getCache("tb_basal_loc");
            if(cache instanceof EhCacheCache){
                Ehcache ehcache =((EhCacheCache)cache).getNativeCache();
            }
        }
        return null;
    }
}
