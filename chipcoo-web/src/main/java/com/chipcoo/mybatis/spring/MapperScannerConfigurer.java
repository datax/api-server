package com.chipcoo.mybatis.spring;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.GenericBeanDefinition;

public class MapperScannerConfigurer extends org.mybatis.spring.mapper.MapperScannerConfigurer {
    MapperFactoryBean mapperFactoryBean = new MapperFactoryBean();
    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) {

        super.postProcessBeanDefinitionRegistry(registry);
        String[] beanNames = registry.getBeanDefinitionNames();
        for(int i=0;i<beanNames.length;i++) {
            String beanName =beanNames[i];
            BeanDefinition beanDefinition = registry.getBeanDefinition(beanName);
            if(beanDefinition instanceof GenericBeanDefinition){
                GenericBeanDefinition genericBeanDefinition = (GenericBeanDefinition)beanDefinition;
                if(genericBeanDefinition.getBeanClassName().equals(org.mybatis.spring.mapper.MapperFactoryBean.class.getName())){
                    genericBeanDefinition.setBeanClass(mapperFactoryBean.getClass());
                }
            }


        }
    }
}
