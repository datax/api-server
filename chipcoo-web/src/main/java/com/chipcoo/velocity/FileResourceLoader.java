package com.chipcoo.velocity;

import org.apache.velocity.exception.ResourceNotFoundException;

import java.io.InputStream;

public class FileResourceLoader extends org.apache.velocity.runtime.resource.loader.FileResourceLoader {


    @Override
    public InputStream getResourceStream(String templateName) throws ResourceNotFoundException {
        if(null!=templateName&& templateName.length()>0) templateName=templateName.toLowerCase();
        return super.getResourceStream(templateName);
    }
}