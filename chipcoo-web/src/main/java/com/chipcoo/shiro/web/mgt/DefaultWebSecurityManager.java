package com.chipcoo.shiro.web.mgt;

import com.chipcoo.shiro.authz.PermissionResolver;
import org.apache.shiro.authc.Authenticator;
import org.apache.shiro.authc.LogoutAware;
import org.apache.shiro.authz.ModularRealmAuthorizer;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;

public class DefaultWebSecurityManager extends org.apache.shiro.web.mgt.DefaultWebSecurityManager {
    public DefaultWebSecurityManager() {
        super();
        ModularRealmAuthorizer authorizer = new ModularRealmAuthorizer();
        authorizer.setPermissionResolver(new PermissionResolver());

        super.setAuthorizer(authorizer);
    }

    /**
     * 不删除SESSION，仅删除凭证
     * @param subject
     */
    @Override
    public void logout(Subject subject) {
        if (subject == null) throw new IllegalArgumentException("Subject method argument cannot be null.");
        beforeLogout(subject);

        PrincipalCollection principals = subject.getPrincipals();
        if (principals != null && !principals.isEmpty()) {
            Authenticator authc = getAuthenticator();
            if (authc instanceof LogoutAware){((LogoutAware) authc).onLogout(principals);}
        }

        try {
            delete(subject); //remove from session attribute
        } catch (Exception e) {
            String msg = "Unable to cleanly unbind Subject.  Ignoring (logging out).";

        } finally {
            try {
                stopSession(subject);
            } catch (Exception e) {
                String msg = "Unable to cleanly stop Session for Subject [" + subject.getPrincipal() + "] " +
                        "Ignoring (logging out).";

            }
        }
    }
}