package com.chipcoo.shiro.spring.web;

import org.apache.shiro.util.PatternMatcher;
import org.springframework.util.AntPathMatcher;

public class AntPathIgnoreCaseMatcher extends AntPathMatcher implements PatternMatcher {
    public AntPathIgnoreCaseMatcher(){
        super();

        super.setCaseSensitive(false);
    }
    @Override
    public boolean matches(String pattern, String source) {
        return super.match(pattern,source);
    }
}
