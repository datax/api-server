package com.chipcoo.shiro.mgt;

import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.mgt.CookieRememberMeManager;

public class RememberMeManager extends CookieRememberMeManager {
    public RememberMeManager(){
        super();
        super.setSerializer(new PrincipalSerializer());
    }
    @Override
    protected void rememberSerializedIdentity(Subject subject, byte[] serialized) {
        super.rememberSerializedIdentity(subject, serialized);
    }
}
