package com.chipcoo.shiro.session.mgt;

import org.apache.shiro.session.mgt.SimpleSession;

/**
 * Created by Administrator on 2016/5/8.
 */
public class UserSession extends SimpleSession {
    private String serverHost;
    public UserSession(){

    }
    public UserSession(String host,String serverHost){
        super(host);
        setServerHost(serverHost);
    }
    public String getServerHost() {
        return serverHost;
    }

    public void setServerHost(String serverHost) {
        this.serverHost = serverHost;
    }



    public String getFingerID() {
        Object o =  getAttribute("finger_id");
        return o==null?null:o.toString();
    }

    public void setFingerID(String fingerID) {
        Object o =  getAttribute("finger_id");
        if(null==fingerID||fingerID.length()<1){
            if(null!=o) removeAttribute("finger_id");
        }else{
            if(null==o) setAttribute("finger_id",fingerID);
            else if(!fingerID.equals(o.toString()))
                setAttribute("finger_id",fingerID);
        }

    }


}
