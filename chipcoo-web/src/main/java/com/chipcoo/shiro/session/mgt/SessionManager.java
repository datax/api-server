package com.chipcoo.shiro.session.mgt;

import com.chipcoo.services.ConfService;
import org.apache.shiro.session.Session;
import org.apache.shiro.session.UnknownSessionException;
import org.apache.shiro.session.mgt.SessionKey;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

public class SessionManager extends DefaultWebSessionManager {
    public SessionManager() {
        setSessionFactory(new UserSessionFactory());
    }

    @Override
    protected void afterSessionValidationEnabled() {
        super.afterSessionValidationEnabled();
        confService.removeDirtySessions( getGlobalSessionTimeout() );
    }

    @Autowired
    private ConfService confService;

    /*@Override
    protected Session retrieveSession(SessionKey sessionKey) throws UnknownSessionException {
        System.err.println("===================retrieveSession");
        Session session =  super.retrieveSession(sessionKey);

        return session;
    }

    @Override
    protected Session retrieveSessionFromDataSource(Serializable sessionId) throws UnknownSessionException {
        System.err.println("===================retrieveSessionFromDataSource");
        Session session =  super.retrieveSessionFromDataSource(sessionId);

        return session;
    }*/


}
