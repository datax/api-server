package com.chipcoo.shiro.session.mgt;

import org.apache.shiro.session.Session;
import org.apache.shiro.session.mgt.SessionContext;
import org.apache.shiro.session.mgt.SimpleSessionFactory;
import org.apache.shiro.web.session.mgt.DefaultWebSessionContext;

import javax.servlet.ServletRequest;

/**
 * Created by Administrator on 2016/5/8.
 */
public class UserSessionFactory extends SimpleSessionFactory {
    @Override
    public Session createSession(SessionContext initData) {
        //return super.createSession(initData);
        if (initData != null) {
            String host = initData.getHost();
            String serverHost = null;
            if(initData instanceof DefaultWebSessionContext){
                ServletRequest servletRequest = ((DefaultWebSessionContext)initData).getServletRequest();
                serverHost = servletRequest.getServerName();
            }
            if (host != null) {
                return new UserSession(host,serverHost);
            }
        }
        return new UserSession();
    }
}
