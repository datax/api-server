package com.chipcoo.shiro.session.mgt;

import com.chipcoo.utils.CryptoUtils;
import org.apache.shiro.session.Session;

import java.io.Serializable;
import java.util.UUID;

public final class SessionIdGenerator implements org.apache.shiro.session.mgt.eis.SessionIdGenerator {
    @Override
    public Serializable generateId(Session session) {
        String uuid = UUID.randomUUID().toString();
        String sessionId = CryptoUtils.murmurhash3_x64_128(uuid);
        return sessionId;
    }
}