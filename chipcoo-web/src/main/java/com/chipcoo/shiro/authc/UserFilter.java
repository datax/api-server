package com.chipcoo.shiro.authc;

import com.chipcoo.ResponseResult;
import com.chipcoo.web.utils.WebUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public final class UserFilter  extends org.apache.shiro.web.filter.authc.UserFilter {
    @Override
    protected void redirectToLogin(ServletRequest request, ServletResponse response) throws IOException {

        /*if(request instanceof HttpServletRequest){*/
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;

        String returnUrl = httpServletRequest.getRequestURI();
        String loginUrl = getLoginUrl();

        ResponseResult responserResult = ResponseResult.createError(ResponseResult.REQUIRED_LOGIN)
                .redirect(loginUrl)
                .put("returnUrl", returnUrl);

        WebUtils.responseEnd(httpServletRequest, (HttpServletResponse) response, responserResult);
           /* if(com.chipcoo.web.utils.MediaTypeUtils.isAjax(httpServletRequest)){

                ResponseResult responseResult = ResponseResult.createError(ResponseResult.REQUIRED_LOGIN);
                com.chipcoo.web.utils.WebUtils.responseJSONEnd( httpServletRequest,
                            (HttpServletResponse) response,
                            responseResult
                        );
            }
            else WebUtils.issueRedirect(request, response, String.format("%s?returnUrl=%s",loginUrl,returnUrl));*/
        /*}
        else super.redirectToLogin(request,response);*/

    }
}
