package com.chipcoo.shiro.authc;

import com.chipcoo.ResponseResult;
import com.chipcoo.utils.StringUtils;
import com.chipcoo.web.utils.WebUtils;
import org.apache.shiro.web.filter.AccessControlFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by Administrator on 2016/5/8.
 */
public final class GuestFilter extends AccessControlFilter {
    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) throws Exception {
        return !getSubject(request, response).isAuthenticated();
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String referer =com.chipcoo.web.utils.WebUtils.getURLReferer(httpServletRequest);
        if(StringUtils.isEmpty(referer)) referer = "/";
        ResponseResult responseResult = ResponseResult.createError(ResponseResult.GUEST_ONLY).redirect(referer);
        WebUtils.responseEnd(httpServletRequest,(HttpServletResponse) response,responseResult);
        return false;
    }
}
