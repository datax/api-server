package com.chipcoo.shiro.authz;

import org.apache.shiro.authz.Permission;
import org.apache.shiro.authz.permission.WildcardPermission;

public class PermissionResolver implements org.apache.shiro.authz.permission.PermissionResolver {
    @Override
    public Permission resolvePermission(String permissionString) {
        if(permissionString.startsWith("+"))
            return  new BitPermission(permissionString);
        return new WildcardPermission(permissionString);
    }
}
