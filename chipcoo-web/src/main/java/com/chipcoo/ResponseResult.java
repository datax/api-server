package com.chipcoo;

import java.util.HashMap;
import java.util.Map;

public final class ResponseResult<TData> {
    public static String KEY_REDIRECT = "redirect";
    public static ResponseResult<?> createError(int errCode){

        ResponseResult ret;
        if(errCode == ERR_BULK){
            ret = new ResponseResult<HashMap<String,Object>>();
        }
        else
            ret = new ResponseResult();

        ret.setCode(errCode);
        return ret;
    }

    public final static Integer REQUIRED_FINGER = 1;    //需要提交finger
    public final static Integer NOT_SUPPORT_COOKIE = 2; //检测到客户不支持cookie

    public final static Integer REQUIRED_CAPTCHA = 3;   //需要验证码
    public final static Integer REQUIRED_LOGIN = 4;     //需要登录

    public final static Integer NOT_FIND = 10;
    public final static Integer DISABLED = 11;
    public final static Integer LOCKED = 12;

    public final static Integer EXPIRED = 13;
    public final static Integer TOO_FAST = 14;


    public final static Integer ERR_BULK = 100;        //组合错误，错误详情参考DATA，例如：登录时,{code:100,data:{password:104}} 表示 密码错误
    public final static Integer ERR_CAPTCHA = 101;    //验证码错误
    public final static Integer ERR_FORMAT  = 102;    //格式错误
    public final static Integer ERR_EXISTS = 103;     //实体存在，例如账号
    public final static Integer ERR_VAL  = 104;       //值无效

    public final static Integer GUEST_ONLY = 200;       //只允许游客访问
    public final static Integer NO_PERM = 201;        //没有相应权限或许可操作


    public final static Integer ERR_SYSTEM = -1;      //应用错误
    public final static Integer ERR_DB = -2;
    public final static Integer ERR_NET = -3;
    public final static Integer ERR_IO = -4;

    public final static Integer MSG_CUSTOM = -99;


    private int code;
    private String message;
    private TData data;

    public ResponseResult(){}
    public ResponseResult(TData data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public ResponseResult<?> setCode(int code) {
        this.code = code;
        return this;
    }

    public TData getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        if(null!=message&&message.length()>0) {
            this.message = message;
            this.code = MSG_CUSTOM;
        }
    }

    public ResponseResult<?> put(String key,Object val){
        if(null== this.data)
            this.data = (TData) new HashMap<String,Object>();
        if(this.data instanceof Map){
            if(null!=key&&key.length()>0) {
                ((Map) this.data).put(key, val);
            }
        }
        return this;
    }
    public Object get(String key){
        if(this.data instanceof Map){
            return ((Map)this.data).get(key);
        }
        return null;
    }
    public ResponseResult<?> redirect(String url){
        if(null==url) {
            if (null != get(KEY_REDIRECT))
                ((Map) this.data).remove(KEY_REDIRECT);
            return this;
        }
        return put(KEY_REDIRECT,url);
    }
}

