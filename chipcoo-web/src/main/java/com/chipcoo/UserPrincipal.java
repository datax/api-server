package com.chipcoo;

import com.chipcoo.models.gen.User;
import com.chipcoo.models.gen.UserProfile;

public class UserPrincipal implements IUserPrincipal {

    public UserPrincipal(String loginName, User user, UserProfile profile){
        this.userID = user.getId();
        this.loginName = loginName;

    }

    private int userID;
    private String loginName,nickname;
    public int getUserID() {
        return userID;
    }
    public String getLoginName() {
        return loginName;
    }
    public String getNickname() {
        return nickname;
    }





}
