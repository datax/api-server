package com.chipcoo.services;

import com.chipcoo.dao.impl.BioEduMapperImpl;
import com.chipcoo.dao.impl.BioExpMapperImpl;
import com.chipcoo.dao.impl.BioJobMapperImpl;
import com.chipcoo.models.gen.BioEdu;
import com.chipcoo.models.gen.BioExp;
import com.chipcoo.models.gen.BioJob;
import com.xcode.mybatis.mapper.Example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 个人档案服务
 */
@Service

public class BIOService{
    @Autowired
    private BioJobMapperImpl bioJobMapper;
    @Autowired
    private BioEduMapperImpl bioEduMapper;
    @Autowired
    private BioExpMapperImpl bioExpMapper;

    public BioJob jobGet(Integer id){
        BioJob o = bioJobMapper.selectByPrimaryKey(id);
        if(null!=o && o.getDeleted())
            return null;
        return o;
    }
    public List jobSelectByUserID(Integer userID){
        Example example = new Example();
        example.createCriteria()
                .eq("userID",userID)
                .eq("deleted",false);

        return bioJobMapper.selectByExample(example);
    }
    public int jobDelete(Integer id){
        return bioJobMapper.deleteByPrimaryKey(id);
    }
    public void jobCreateUpdate(BioJob model){
       if(null!=model.getId()&&model.getId()>0){
           bioJobMapper.updateByPrimaryKeySelective(model);
       }else {
           bioJobMapper.insertSelective(model);
       }
    }


    public BioEdu eduGet(Integer id){
        BioEdu o = bioEduMapper.selectByPrimaryKey(id);
        if(null!=o && o.getDeleted())
            return null;
        return o;
    }
    public List eduSelectByUserID(Integer userID){
        Example example = new Example();
        example.createCriteria()
                .eq("userID",userID)
                .eq("deleted",false);

        return bioEduMapper.selectByExample(example);
    }
    public int eduDelete(Integer id){
        return bioEduMapper.deleteByPrimaryKey(id);
    }
    public void eduCreateUpdate(BioEdu model){
        if(null!=model.getId()&&model.getId()>0){
            bioEduMapper.updateByPrimaryKeySelective(model);
        }else {
            bioEduMapper.insertSelective(model);
        }
    }

    public BioExp expGet(Integer id){
        BioExp o = bioExpMapper.selectByPrimaryKey(id);
        if(null!=o && o.getDeleted())
            return null;
        return o;
    }
    public List expSelectByUserID(Integer userID){
        Example example = new Example();
        example.createCriteria()
                .eq("userID",userID)
                .eq("deleted",false);

        return bioExpMapper.selectByExample(example);
    }
    public int expDelete(Integer id){
        return bioExpMapper.deleteByPrimaryKey(id);
    }
    public void expCreateUpdate(BioExp model){
        if(null!=model.getId()&&model.getId()>0){
            bioExpMapper.updateByPrimaryKeySelective(model);
        }else {
            bioExpMapper.insertSelective(model);
        }
    }
}