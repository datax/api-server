package com.chipcoo.services;

import com.chipcoo.email.impl.SendCloudEmailImpl;
import com.chipcoo.sms.impl.SendCloudSMSImpl;
import com.chipcoo.utils.SecurityUtils;
import com.chipcoo.utils.StringUtils;
import com.chipcoo.ResponseResult;
import org.apache.shiro.session.Session;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;

@Service
public class MessageService {
    public int getCodeStatus(String codeUserPost) {
        if(null==codeUserPost||codeUserPost.length()==0) return ResponseResult.REQUIRED_CAPTCHA;
        Session session = SecurityUtils.getSession(false);
        if(null==session) return ResponseResult.REQUIRED_FINGER;
        Object oSmsAt = session.getAttribute("sms_at");
        if (null == oSmsAt)
            return ResponseResult.EXPIRED;

        Date dtSmsAt = (Date) oSmsAt;
        if ((new Date().getTime() - dtSmsAt.getTime()) > 5 * 60 * 1000)
            return ResponseResult.EXPIRED;

        String code = session.getAttribute("sms_code").toString();
        if (!code.equalsIgnoreCase(codeUserPost))
            return ResponseResult.ERR_CAPTCHA;

        return  0;
    }
    private int beforeCodeSend() {
        Session session = SecurityUtils.getSession(false);
        if (null == session) return ResponseResult.REQUIRED_FINGER;

        Object oSmsAt = session.getAttribute("sms_at");
        if (null != oSmsAt) {
            Date dtSmsAt = (Date) oSmsAt;
            if ((new Date().getTime() - dtSmsAt.getTime()) < 60 * 1000) {
                return ResponseResult.TOO_FAST;
            }
        }
        return 0;

    }
    public int sendRegisterCode(boolean isPhone, String username) {
        int iErr = beforeCodeSend();
        if(iErr>0) return  iErr;

        Session session = SecurityUtils.getSession(false);
        int sms_code = StringUtils.nextInt(100000, 999999);
        session.setAttribute("sms_code", sms_code);
        session.setAttribute("sms_at", new Date());
        if (isPhone) {
            SendCloudSMSImpl sendCloud = new SendCloudSMSImpl();
            HashMap<String, String> vars = new HashMap<>();
            vars.put("code", String.valueOf(sms_code));
            sendCloud.sendTemplate(1069, vars, username);
        } else {
            SendCloudEmailImpl sendCloudEmail = new SendCloudEmailImpl();
            HashMap<String, String> vars = new HashMap<>();
            vars.put("name", "亲");
            vars.put("code", String.valueOf(sms_code));
            sendCloudEmail.sendTemplate("chipcoo_signup", vars, username);
        }
        return 0;
    }
    public  int sendForgotCode( boolean isPhone, String username) {
        int iErr = beforeCodeSend();
        if(iErr>0) return  iErr;
        Session session = SecurityUtils.getSession(false);
        int sms_code = StringUtils.nextInt(100000, 999999);
        session.setAttribute("sms_code", sms_code);
        session.setAttribute("sms_at", new Date());
        if (isPhone) {
            SendCloudSMSImpl sendCloud = new SendCloudSMSImpl();
            HashMap<String, String> vars = new HashMap<>();
            vars.put("code", String.valueOf(sms_code));
            sendCloud.sendTemplate(1070, vars, username);
        } else {
            SendCloudEmailImpl sendCloudEmail = new SendCloudEmailImpl();
            HashMap<String, String> vars = new HashMap<>();
            vars.put("name", "亲");
            vars.put("code", String.valueOf(sms_code));
            sendCloudEmail.sendTemplate("chipcoo_forgotpassword", vars, username);
        }
        return 0;
    }

    public  int sendBindCode(boolean isPhone, String username) {
        int iErr = beforeCodeSend();
        if(iErr>0) return  iErr;
        Session session = SecurityUtils.getSession(false);
        int sms_code = StringUtils.nextInt(100000, 999999);
        session.setAttribute("sms_code", sms_code);
        session.setAttribute("sms_at", new Date());
        if (isPhone) {
            SendCloudSMSImpl sendCloud = new SendCloudSMSImpl();
            HashMap<String, String> vars = new HashMap<>();
            vars.put("code", String.valueOf(sms_code));
            sendCloud.sendTemplate(1070, vars, username);
        } else {
            SendCloudEmailImpl sendCloudEmail = new SendCloudEmailImpl();
            HashMap<String, String> vars = new HashMap<>();
            vars.put("name", "亲");
            vars.put("code", String.valueOf(sms_code));
            sendCloudEmail.sendTemplate("chipcoo_forgotpassword", vars, username);
        }
        return 0;
    }

}
