package com.chipcoo.services;

import com.chipcoo.dao.impl.*;
import com.chipcoo.models.gen.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cglib.core.Local;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConfService {
    @Autowired
    private ConfDicMapperImpl confDicMapper;
    @Autowired
    private ConfSiteMatcherMapperImpl confSiteMatcherMapper;
    @Autowired
    private ConfRedirectRuleMapperImpl confRedirectRuleMapper;
    //region device finger
    @Autowired
    private FingerMapperImpl deviceFingerMapper;

    @Cacheable(value = "tb_conf_dic",key = "#keyName.toLowerCase()+#hostID")
    public ConfDic getConf(String keyName, int hostID){
        ConfDicKey key = new ConfDicKey();
        key.setId(keyName);
        key.setSiteID(hostID);
        ConfDic val= confDicMapper.selectByPrimaryKey(key);
        return val;
    }

    @Cacheable( value = "tb_conf_sitematcher")
    public List<ConfSiteMatcher> selectSiteMatcher(){
        return confSiteMatcherMapper.selectAll();
    }


    @Cacheable( value = "tb_conf_redirectrule")
    public List<ConfRedirectRule> selectRedirectRule(){
        return confRedirectRuleMapper.selectAll();
    }

    @Cacheable(value = "tb_basal_finger")
    public Finger getDeviceFinger(String id){
        return deviceFingerMapper.selectByPrimaryKey(id);
    }
    @CacheEvict(value="tb_basal_finger",key="#item.getId()")
    public void updateDeviceFinger(Finger item){
        deviceFingerMapper.updateByPrimaryKeySelective(item);
    }
    @CacheEvict(value = "tb_basal_finger")
    public void deleteDeviceFinger(String id){
        deviceFingerMapper.deleteByPrimaryKey(id);
    }
    public void insertDeviceFinger(Finger item){
        deviceFingerMapper.insertSelective(item);
    }
    //endregion

    //region session
    @Autowired
    private SessionMapperImpl sessionMapper;
    public int removeDirtySessions(long timeout){
        return sessionMapper.removeDirties( (int)Math.ceil(timeout/1000) );
    }
    //endregion


    @Autowired
    private DicLocMapperImpl locMapper;
    @Cacheable(value = "tb_basal_loc")
    public DicLoc getLoc(int b1,int b2,int b3,int b4){
        DicLocKey key = new DicLocKey();
        key.setB1(b1);
        key.setB2(b2);
        key.setB3(b3);
        key.setB4(b4);
        return locMapper.selectByPrimaryKey(key);
    }
}
