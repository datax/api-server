package com.chipcoo.services;


import me.chanjar.weixin.common.exception.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpInMemoryConfigStorage;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.result.WxMpOAuth2AccessToken;
import me.chanjar.weixin.mp.bean.result.WxMpUser;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

/**
 * 微信服务
 */
@Service
public class WeiXinService {
    private WxMpService wxMpService;
    {
        wxMpService = new WxMpServiceImpl();
        WxMpInMemoryConfigStorage config = new WxMpInMemoryConfigStorage();
        config.setAppId("wxaf4fc10cba78db30"); // 设置微信公众号的appid
        config.setSecret("d4624c36b6795d1d99dcf0547af5443d"); // 设置微信公众号的app corpSecret
        wxMpService.setWxMpConfigStorage(config);
    }
    public WxMpOAuth2AccessToken oauth2getAccessToken(String code)  throws WxErrorException {
        return wxMpService.oauth2getAccessToken(code);
    }

    public WxMpOAuth2AccessToken oauth2getAccessToken(WxMpOAuth2AccessToken token)  throws WxErrorException {
        return  wxMpService.oauth2refreshAccessToken(token.getRefreshToken());
    }

    public WxMpUser oauth2getUserInfo(WxMpOAuth2AccessToken token)  throws WxErrorException{
        return wxMpService.oauth2getUserInfo(token,"zh_CN");
    }
    public WxMpUser oauth2getUserInfo(WxMpOAuth2AccessToken token, String lang)  throws WxErrorException{
        return wxMpService.oauth2getUserInfo(token,lang);
    }
}
