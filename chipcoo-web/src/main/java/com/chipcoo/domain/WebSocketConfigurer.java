package com.chipcoo.domain;


import com.chipcoo.websocket.HandshakeInterceptorImpl;
import com.chipcoo.websocket.WebSocketHandlerEndPoint;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.config.annotation.WebSocketConfigurationSupport;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistration;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

@Configuration
public class WebSocketConfigurer extends WebSocketConfigurationSupport {

    @Override
    protected void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {

        WebSocketHandlerRegistration registration = registry.addHandler(new WebSocketHandlerEndPoint(),"/websocket");
        registration.withSockJS();
        registration.addInterceptors(new HandshakeInterceptorImpl());

        //for debug
        registration.setAllowedOrigins("*");

    }
}
