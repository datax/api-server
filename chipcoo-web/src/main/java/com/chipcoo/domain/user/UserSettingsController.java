package com.chipcoo.domain.user;

import com.chipcoo.IUserPrincipal;
import com.chipcoo.models.gen.Passport;
import com.chipcoo.services.MessageService;
import com.chipcoo.services.UserService;
import com.chipcoo.ResponseResult;
import com.chipcoo.utils.ValidateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 用户为中心(绝对私有）
 * 基本资料变更、ETC...
 */
@Controller
@RequestMapping("usersettings")
public class UserSettingsController {
    @Autowired
    private UserService userService;
    @Autowired
    private MessageService messageService;

    /**
     * 获取当前用户所有的passport
     * @return
     */
    @RequestMapping(value = "passportlist",method = RequestMethod.GET)
    public ResponseResult passportList(){
        int errCode = 0;
        IUserPrincipal userPrincipal = com.chipcoo.utils.SecurityUtils.getCurrentUser();
        List<Passport> passports=userService.selectPassport(userPrincipal.getUserID(),true);
        return   new ResponseResult(passports).setCode(errCode);
    }
    /**
     * 提交账号并获取验证码
     * @return
     */
    @RequestMapping(value = "passportbind",method = RequestMethod.GET)
    public ResponseResult passportBind(HttpServletRequest req){

        return  ResponseResult.createError(0);
    }
    /**
     * 提交验证码并绑定
     * @param username
     * @return
     */
    @RequestMapping(value = "passportbind",method = RequestMethod.POST)
    public ResponseResult passportBind(HttpServletRequest req, @RequestParam(required = false) String username){
        int errCode = 0;
        if(!ValidateUtils.isCaptchaValidated(req)) {
            return ResponseResult.createError(ResponseResult.ERR_CAPTCHA);
        }
        boolean isPhone  = ValidateUtils.isPhone(username);
        boolean isEmail;
        if(!isPhone){
            isEmail = ValidateUtils.isEmail(username);
        }else isEmail=false;

        Passport passport=null;
        if (!isEmail&&!isPhone){
            errCode = ResponseResult.ERR_FORMAT;
        } else {
            if (isPhone) {
                passport = userService.getAuthenticationPassport(username, 1);
            } else {
                passport = userService.getAuthenticationPassport(username, 2);
            }
        }

        if (null!=passport) {
            errCode = ResponseResult.ERR_EXISTS;
        }

        if( errCode==0){
            //业务数据验证成功，开始发送验证码
            errCode = messageService.sendForgotCode(isPhone,username);
        }

        return  ResponseResult.createError(errCode);
    }
    @RequestMapping(value = "passportbinddone",method = RequestMethod.POST)
    public ResponseResult passportbinddone(HttpServletRequest req, @RequestParam(required = false) String username,@RequestParam(required = false) String code){
        int errCode = messageService.getCodeStatus(code);
        if(errCode>0) return ResponseResult.createError(errCode);


        boolean isPhone  = ValidateUtils.isPhone(username);
        boolean isEmail;
        if(!isPhone){
            isEmail = ValidateUtils.isEmail(username);
        }else isEmail=false;

        if (!isEmail&&!isPhone){
            errCode = ResponseResult.ERR_FORMAT;
        } else {
            if( 0 == userService.bindPassport(username,isPhone?1:2,com.chipcoo.utils.SecurityUtils.getCurrentUser().getUserID(),null))
                errCode = ResponseResult.ERR_EXISTS;

        }

        return  ResponseResult.createError(errCode);
    }
    /**
     * 修改密码
     * @param password
     * @return
     */
    @RequestMapping(value = "password",method = RequestMethod.POST)
    public ResponseResult updatePassword(@RequestParam(required = false) String password){
        int errCode = 0;
        if(null==password||password.length()<6) errCode = ResponseResult.ERR_FORMAT;
        else {
            IUserPrincipal userPrincipal = com.chipcoo.utils.SecurityUtils.getCurrentUser();
            userService.changePassword(userPrincipal.getUserID(), password);
        }

        return  ResponseResult.createError(errCode);
    }

    @RequestMapping(value = "{userID:[1-9][0-9]*}/passport/{id:[1-9][0-9]*}/actived",method = RequestMethod.POST)
    public ResponseResult passportActived(@PathVariable Integer id) {
        Passport passport= userService.getAuthenticationPassport(id);
        if(null==passport) return ResponseResult.createError(ResponseResult.NOT_FIND);
        IUserPrincipal userPrincipal = com.chipcoo.utils.SecurityUtils.getCurrentUser();
        if( passport.getUserID()!= userPrincipal.getUserID() ) return ResponseResult.createError(ResponseResult.NO_PERM);
        if( passport.getPassportType()!=1 ) return ResponseResult.createError(ResponseResult.ERR_VAL);


        //todo:设定主帐号
        return null;
    }





}
