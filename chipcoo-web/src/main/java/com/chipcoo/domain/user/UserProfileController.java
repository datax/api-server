package com.chipcoo.domain.user;

import com.chipcoo.IUserPrincipal;
import com.chipcoo.ResponseResult;
import com.chipcoo.dao.gen.BioEduMapper;
import com.chipcoo.dao.gen.BioExpMapper;
import com.chipcoo.dao.gen.BioJobMapper;
import com.chipcoo.models.gen.BioEdu;
import com.chipcoo.models.gen.BioExp;
import com.chipcoo.models.gen.BioJob;
import com.chipcoo.models.gen.UserProfile;
import com.chipcoo.services.BIOService;
import com.chipcoo.services.UserService;
import com.chipcoo.utils.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.ArrayList;
import java.util.List;

/**
 * 用户为中心(彼此可见）
 * 基本资料变更、ETC...
 */
@Controller
@RequestMapping("userprofile")
@RequiresUser
public class UserProfileController {
    @Autowired
    private UserService userService;
    @Autowired
    private BIOService bioService;

    @RequestMapping(value = "",method = RequestMethod.GET)
    public ResponseResult profile(){
        return profile(SecurityUtils.getCurrentUser().getUserID());
    }
    /**
     * 查看用户资料
     * @param userID
     */
    @RequestMapping(value = "{userID:[1-9][0-9]*}",method = RequestMethod.GET)
    public ResponseResult profile(@PathVariable() int userID){
        int errCode = 0;
        UserProfile model = null;
        //todo:权限控制，不是什么人都能请求该资源
        model = userService.getProfile(userID);
        if (null == model) errCode = ResponseResult.NOT_FIND;
        return new ResponseResult(model).setCode(errCode);
    }


    @RequestMapping(value = "",method = RequestMethod.POST)
    public ResponseResult profile(UserProfileViewModel model){
        return profile(SecurityUtils.getCurrentUser().getUserID(),model);
    }
    @RequestMapping(value = "{userID:[1-9][0-9]*}",method = RequestMethod.POST)
    public ResponseResult profile(@PathVariable int userID,UserProfileViewModel model){
        int errCode = 0;


        //todo:权限控制，不是什么人都能请求该资源
        if( userID!=SecurityUtils.getCurrentUser().getUserID()) errCode=ResponseResult.NO_PERM;

        if (0==errCode) {
            model.setUserID(userID);//always set to userid
            userService.createUpdateProfile(model);
        }
        return  ResponseResult.createError(errCode);
    }

    @RequestMapping(value = "{userID:[1-9][0-9]*}/job/{id:[1-9][0-9]*}",method = RequestMethod.GET)
    public ResponseResult jobView(@PathVariable int userID,@PathVariable int id){
        int errCode = 0;
        List<BioJob> list;
        if(id<=0) list = bioService.jobSelectByUserID(userID);
        else {
            list = new ArrayList<BioJob>();
            BioJob o = bioService.jobGet(id);
            if(null!=o) list.add(o);
        }

        return new ResponseResult(list).createError(errCode);
    }
    @RequestMapping(value = "{userID:[1-9][0-9]*}/job/{id:[1-9][0-9]*}/delete",method = RequestMethod.POST)
    public ResponseResult jobDelete(@PathVariable int userID,@PathVariable int id){
        int errCode = 0;
        //TODO: 2016/5/12
        if(userID!=SecurityUtils.getCurrentUser().getUserID()) errCode = ResponseResult.NO_PERM;

        if(0==errCode){
            bioService.jobDelete(id);
        }
        return  ResponseResult.createError(errCode);
    }
    @RequestMapping(value = "{userID:[1-9][0-9]*}/job/{id:[1-9][0-9]*}/edit",method = RequestMethod.POST)
    public ResponseResult jobEdit(@PathVariable int userID,@PathVariable int id,BioJob model){
        int errCode = 0;
        //TODO: 2016/5/12
        if(userID!=SecurityUtils.getCurrentUser().getUserID()) errCode = ResponseResult.NO_PERM;
        if(0==errCode){
            model.setId(id);
            if(id<1) {
                model.setUserID(userID);
            }
            bioService.jobCreateUpdate(model);
        }
        return  ResponseResult.createError(errCode);
    }
    @RequestMapping(value = "{userID:[1-9][0-9]*}/job/create",method = RequestMethod.POST)
    public ResponseResult jobCreate(@PathVariable int userID,BioJob model){
        return jobEdit(SecurityUtils.getCurrentUser().getUserID(),model);
    }
    @RequestMapping(value = "{userID:[1-9][0-9]*}/job/",method = RequestMethod.GET)
    public ResponseResult jobView(@PathVariable int userID){
        return jobView(userID,0);
    }
    @RequestMapping(value = "job",method = RequestMethod.GET)
    public ResponseResult jobViewSelf(){
        return jobView(SecurityUtils.getCurrentUser().getUserID(),0);
    }
    @RequestMapping(value = "job/{id:[1-9][0-9]*}",method = RequestMethod.GET)
    public ResponseResult jobViewSelf(@PathVariable int id){
        return jobView(SecurityUtils.getCurrentUser().getUserID(),id);
    }
    @RequestMapping(value = "job/{id:[1-9][0-9]*}/delete",method = RequestMethod.POST)
    public ResponseResult jobDelete(@PathVariable int id){
        return jobDelete(SecurityUtils.getCurrentUser().getUserID(),id);
    }
    @RequestMapping(value = "job/{id:[1-9][0-9]*}/edit",method = RequestMethod.POST)
    public ResponseResult jobEdit(@PathVariable int id, BioJob model){
        return jobEdit(SecurityUtils.getCurrentUser().getUserID(),model);
    }
    @RequestMapping(value = "job/create",method = RequestMethod.POST)
    public ResponseResult jobCreate(BioJob model){
        return jobCreate(SecurityUtils.getCurrentUser().getUserID(),model);
    }


    @RequestMapping(value = "{userID:[1-9][0-9]*}/edu/{id:[1-9][0-9]*}",method = RequestMethod.GET)
    public ResponseResult eduView(@PathVariable int userID,@PathVariable int id){
        int errCode = 0;
        List<BioEdu> list;
        if(id<=0) list = bioService.eduSelectByUserID(userID);
        else {
            list = new ArrayList<BioEdu>();
            BioEdu o = bioService.eduGet(id);
            if(null!=o) list.add(o);
        }

        return new ResponseResult(list).createError(errCode);
    }
    @RequestMapping(value = "{userID:[1-9][0-9]*}/edu/{id:[1-9][0-9]*}/delete",method = RequestMethod.POST)
    public ResponseResult eduDelete(@PathVariable int userID,@PathVariable int id){
        int errCode = 0;
        //TODO: 2016/5/12
        if(userID!=SecurityUtils.getCurrentUser().getUserID()) errCode = ResponseResult.NO_PERM;

        if(0==errCode){
            bioService.eduDelete(id);
        }
        return  ResponseResult.createError(errCode);
    }
    @RequestMapping(value = "{userID:[1-9][0-9]*}/edu/{id:[1-9][0-9]*}/edit",method = RequestMethod.POST)
    public ResponseResult eduEdit(@PathVariable int userID,@PathVariable int id,BioEdu model){
        int errCode = 0;
        //TODO: 2016/5/12
        if(userID!=SecurityUtils.getCurrentUser().getUserID()) errCode = ResponseResult.NO_PERM;
        if(0==errCode){
            model.setId(id);
            if(id<1) {
                model.setUserID(userID);
            }
            bioService.eduCreateUpdate(model);
        }
        return  ResponseResult.createError(errCode);
    }
    @RequestMapping(value = "{userID:[1-9][0-9]*}/edu/create",method = RequestMethod.POST)
    public ResponseResult eduCreate(@PathVariable int userID,BioEdu model){
        return eduEdit(SecurityUtils.getCurrentUser().getUserID(),model);
    }
    @RequestMapping(value = "{userID:[1-9][0-9]*}/edu/",method = RequestMethod.GET)
    public ResponseResult eduView(@PathVariable int userID){
        return eduView(userID,0);
    }
    @RequestMapping(value = "edu",method = RequestMethod.GET)
    public ResponseResult eduViewSelf(){
        return eduView(SecurityUtils.getCurrentUser().getUserID(),0);
    }
    @RequestMapping(value = "edu/{id:[1-9][0-9]*}",method = RequestMethod.GET)
    public ResponseResult eduViewSelf(@PathVariable int id){
        return eduView(SecurityUtils.getCurrentUser().getUserID(),id);
    }
    @RequestMapping(value = "edu/{id:[1-9][0-9]*}/delete",method = RequestMethod.POST)
    public ResponseResult eduDelete(@PathVariable int id){
        return eduDelete(SecurityUtils.getCurrentUser().getUserID(),id);
    }
    @RequestMapping(value = "edu/{id:[1-9][0-9]*}/edit",method = RequestMethod.POST)
    public ResponseResult eduEdit(@PathVariable int id, BioEdu model){
        return eduEdit(SecurityUtils.getCurrentUser().getUserID(),model);
    }
    @RequestMapping(value = "edu/create",method = RequestMethod.POST)
    public ResponseResult eduCreate(BioEdu model){
        return eduCreate(SecurityUtils.getCurrentUser().getUserID(),model);
    }


    @RequestMapping(value = "{userID:[1-9][0-9]*}/exp/{id:[1-9][0-9]*}",method = RequestMethod.GET)
    public ResponseResult expView(@PathVariable int userID,@PathVariable int id){
        int errCode = 0;
        List<BioExp> list;
        if(id<=0) list = bioService.expSelectByUserID(userID);
        else {
            list = new ArrayList<BioExp>();
            BioExp o = bioService.expGet(id);
            if(null!=o) list.add(o);
        }

        return new ResponseResult(list).createError(errCode);
    }
    @RequestMapping(value = "{userID:[1-9][0-9]*}/exp/{id:[1-9][0-9]*}/delete",method = RequestMethod.POST)
    public ResponseResult expDelete(@PathVariable int userID,@PathVariable int id){
        int errCode = 0;
        //TODO: 2016/5/12
        if(userID!=SecurityUtils.getCurrentUser().getUserID()) errCode = ResponseResult.NO_PERM;

        if(0==errCode){
            bioService.expDelete(id);
        }
        return  ResponseResult.createError(errCode);
    }
    @RequestMapping(value = "{userID:[1-9][0-9]*}/exp/{id:[1-9][0-9]*}/edit",method = RequestMethod.POST)
    public ResponseResult expEdit(@PathVariable int userID,@PathVariable int id,BioExp model){
        int errCode = 0;
        //TODO: 2016/5/12
        if(userID!=SecurityUtils.getCurrentUser().getUserID()) errCode = ResponseResult.NO_PERM;
        if(0==errCode){
            model.setId(id);
            if(id<1) {
                model.setUserID(userID);
            }
            bioService.expCreateUpdate(model);
        }
        return  ResponseResult.createError(errCode);
    }
    @RequestMapping(value = "{userID:[1-9][0-9]*}/exp/create",method = RequestMethod.POST)
    public ResponseResult expCreate(@PathVariable int userID,BioExp model){
        return expEdit(SecurityUtils.getCurrentUser().getUserID(),model);
    }
    @RequestMapping(value = "{userID:[1-9][0-9]*}/exp/",method = RequestMethod.GET)
    public ResponseResult expView(@PathVariable int userID){
        return expView(userID,0);
    }
    @RequestMapping(value = "exp",method = RequestMethod.GET)
    public ResponseResult expViewSelf(){
        return expView(SecurityUtils.getCurrentUser().getUserID(),0);
    }
    @RequestMapping(value = "exp/{id:[1-9][0-9]*}",method = RequestMethod.GET)
    public ResponseResult expViewSelf(@PathVariable int id){
        return expView(SecurityUtils.getCurrentUser().getUserID(),id);
    }
    @RequestMapping(value = "exp/{id:[1-9][0-9]*}/delete",method = RequestMethod.POST)
    public ResponseResult expDelete(@PathVariable int id){
        return expDelete(SecurityUtils.getCurrentUser().getUserID(),id);
    }
    @RequestMapping(value = "exp/{id:[1-9][0-9]*}/edit",method = RequestMethod.POST)
    public ResponseResult expEdit(@PathVariable int id, BioExp model){
        return expEdit(SecurityUtils.getCurrentUser().getUserID(),model);
    }
    @RequestMapping(value = "exp/create",method = RequestMethod.POST)
    public ResponseResult expCreate(BioExp model){
        return expCreate(SecurityUtils.getCurrentUser().getUserID(),model);
    }


}
