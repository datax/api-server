package com.chipcoo.domain;

import com.chipcoo.utils.BeanUtils;
import com.chipcoo.web.method.annotation.ResponseResultProcessor;
import com.chipcoo.web.utils.WebUtils;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.support.AllEncompassingFormHttpMessageConverter;
import org.springframework.http.converter.xml.SourceHttpMessageConverter;
import org.springframework.util.PathMatcher;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import javax.xml.transform.Source;
import java.util.List;

@Configuration
public class ApplicationConfigurer extends WebMvcConfigurationSupport
{
    //region 消息处理器
    @Override
    protected void configureMessageConverters(List<HttpMessageConverter<?>> messageConverters) {
        StringHttpMessageConverter stringConverter = new StringHttpMessageConverter();
        stringConverter.setWriteAcceptCharset(false);

        messageConverters.add(new ByteArrayHttpMessageConverter());
        messageConverters.add(stringConverter);
        messageConverters.add(new ResourceHttpMessageConverter());
        messageConverters.add(new SourceHttpMessageConverter<Source>());
        messageConverters.add(new AllEncompassingFormHttpMessageConverter());


    }
    //endregion


    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.setPathMatcher(BeanUtils.getBean("pathMatching",PathMatcher.class));
    }
    //endregion

    //region 配置拦截器
    /*@Bean(name="coreInterceptor")
    public CoreInterceptor coreInterceptor(){
        return new CoreInterceptor();
    }

    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(coreInterceptor());
    }*/
    //endregion

    //region handler Return Value 处理器
    @Override
    protected void addReturnValueHandlers(List<HandlerMethodReturnValueHandler> returnValueHandlers) {
        returnValueHandlers.add(new ResponseResultProcessor());
    }
    //endregion

}
