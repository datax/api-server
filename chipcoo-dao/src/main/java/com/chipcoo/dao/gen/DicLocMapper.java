package com.chipcoo.dao.gen;

import com.chipcoo.models.gen.DicLoc;
import com.chipcoo.models.gen.DicLocKey;
import com.xcode.mybatis.mapper.SimpleMapper;

public interface DicLocMapper extends SimpleMapper<DicLoc, DicLocKey> {
}