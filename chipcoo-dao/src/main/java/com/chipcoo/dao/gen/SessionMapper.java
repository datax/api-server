package com.chipcoo.dao.gen;

import com.chipcoo.models.gen.Session;
import com.xcode.mybatis.mapper.SimpleMapper;

public interface SessionMapper extends SimpleMapper<Session, String> {
}