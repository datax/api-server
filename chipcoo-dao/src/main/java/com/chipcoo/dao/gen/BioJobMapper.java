package com.chipcoo.dao.gen;

import com.chipcoo.models.gen.BioJob;
import com.xcode.mybatis.mapper.SimpleMapper;
import com.xcode.mybatis.mapper.select.SelectByExampleMapper;

public interface BioJobMapper extends SelectByExampleMapper, SimpleMapper<BioJob, Integer> {
}