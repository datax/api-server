package com.chipcoo.dao.gen;

import com.chipcoo.models.gen.ConfSiteMatcher;
import com.xcode.mybatis.mapper.SimpleMapper;
import com.xcode.mybatis.mapper.select.SelectAllMapper;

public interface ConfSiteMatcherMapper extends SelectAllMapper, SimpleMapper<ConfSiteMatcher, Integer> {
}