package com.chipcoo.dao.gen;

import com.chipcoo.models.gen.UserProfile;
import com.xcode.mybatis.mapper.SimpleMapper;
import com.xcode.mybatis.mapper.select.SelectByExampleMapper;
import com.xcode.mybatis.mapper.select.SelectByExampleWithBLOBsMapper;

public interface UserProfileMapper extends SelectByExampleMapper, SimpleMapper<UserProfile, Integer>, SelectByExampleWithBLOBsMapper {
}