package com.chipcoo.dao.gen;

import com.chipcoo.models.gen.BioEdu;
import com.xcode.mybatis.mapper.SimpleMapper;
import com.xcode.mybatis.mapper.select.SelectByExampleMapper;

public interface BioEduMapper extends SelectByExampleMapper, SimpleMapper<BioEdu, Integer> {
}