package com.chipcoo.dao.gen;

import com.chipcoo.models.gen.Finger;
import com.xcode.mybatis.mapper.SimpleMapper;

public interface FingerMapper extends SimpleMapper<Finger, String> {
}