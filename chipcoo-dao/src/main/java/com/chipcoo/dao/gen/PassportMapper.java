package com.chipcoo.dao.gen;

import com.chipcoo.models.gen.Passport;
import com.xcode.mybatis.mapper.SimpleMapper;
import com.xcode.mybatis.mapper.select.SelectByExampleMapper;
import com.xcode.mybatis.mapper.select.SelectByExampleWithBLOBsMapper;

public interface PassportMapper extends SelectByExampleMapper, SimpleMapper<Passport, Integer>, SelectByExampleWithBLOBsMapper {
}