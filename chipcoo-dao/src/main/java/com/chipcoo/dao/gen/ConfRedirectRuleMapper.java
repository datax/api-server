package com.chipcoo.dao.gen;

import com.chipcoo.models.gen.ConfRedirectRule;
import com.xcode.mybatis.mapper.SimpleMapper;
import com.xcode.mybatis.mapper.select.SelectAllMapper;

public interface ConfRedirectRuleMapper extends SelectAllMapper, SimpleMapper<ConfRedirectRule, Integer> {
}