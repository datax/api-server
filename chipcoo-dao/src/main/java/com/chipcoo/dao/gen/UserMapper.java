package com.chipcoo.dao.gen;

import com.chipcoo.models.gen.User;
import com.xcode.mybatis.mapper.SimpleMapper;
import com.xcode.mybatis.mapper.select.SelectByExampleMapper;

public interface UserMapper extends SelectByExampleMapper, SimpleMapper<User, Integer> {
}