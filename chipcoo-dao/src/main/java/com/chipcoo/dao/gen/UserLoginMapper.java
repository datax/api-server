package com.chipcoo.dao.gen;

import com.chipcoo.models.gen.UserLogin;
import com.xcode.mybatis.mapper.SimpleMapper;

public interface UserLoginMapper extends SimpleMapper<UserLogin, Integer> {
}