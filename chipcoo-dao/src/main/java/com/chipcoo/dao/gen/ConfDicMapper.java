package com.chipcoo.dao.gen;

import com.chipcoo.models.gen.ConfDic;
import com.chipcoo.models.gen.ConfDicKey;
import com.xcode.mybatis.mapper.SimpleMapper;

public interface ConfDicMapper extends SimpleMapper<ConfDic, ConfDicKey> {
}