package com.chipcoo.dao.gen;

import com.chipcoo.models.gen.BioExp;
import com.xcode.mybatis.mapper.SimpleMapper;
import com.xcode.mybatis.mapper.select.SelectByExampleMapper;
import com.xcode.mybatis.mapper.select.SelectByExampleWithBLOBsMapper;

public interface BioExpMapper extends SelectByExampleMapper, SimpleMapper<BioExp, Integer>, SelectByExampleWithBLOBsMapper {
}