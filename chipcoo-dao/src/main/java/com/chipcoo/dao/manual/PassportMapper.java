package com.chipcoo.dao.manual;

import com.chipcoo.models.gen.Passport;
import org.apache.ibatis.annotations.Param;

import java.awt.*;
import java.util.*;

/**
 * Created by Administrator on 2016/5/3.
 */
public interface PassportMapper {
    Passport selectByPrincipalAndType(@Param("principal") String principal, @Param("passportType") int passportType);

    //通过UserID以及Type更新数据
    int updateByUserIdAndType(Passport passport);
}
